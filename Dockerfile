FROM alpine:3.7
WORKDIR /
RUN apk add --no-cache --virtual python2
ADD dockertags /usr/local/bin/dockertags
ADD VERSION .

ENTRYPOINT ["dockertags"]

