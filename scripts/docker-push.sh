#!/usr/bin/env bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
source config.env.sh

cd ..
VERSION=$(cat VERSION)
VERSION_TAG=${RELEASE_TAG_PREFIX}${VERSION}${RELEASE_TAG_POSTFIX}
docker push ${IMAGE_URI}:${VERSION_TAG}
docker push ${IMAGE_URI}:latest

