#!/usr/bin/env sh
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

source config.env.sh

function log_info {
  echo "🚀  $1"
}

cd ..
docker run --rm -it -v $PWD:/app -w /app treeder/bump > /dev/null

# reread new version
VERSION=$(cat VERSION)
VERSION_MSG="version: $VERSION"
log_info 'Bumped version to: '$VERSION

# Create commit

git add -A
git commit -m "$VERSION_MSG" > /dev/null
log_info 'Created commit'

# Create tag
VERSION_TAG=${RELEASE_TAG_PREFIX}${VERSION}${RELEASE_TAG_POSTFIX}
git tag -a "$VERSION_TAG" -m "$VERSION_MSG" > /dev/null
log_info 'Created tag: '$VERSION_TAG

# Push tag
git push $GIT_REMOTE $VERSION_TAG 2> /dev/null
log_info "Pushed tag to: ${GIT_REMOTE}."

cd $DIR
./docker-build.sh
./docker-push.sh

