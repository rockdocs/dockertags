#!/usr/bin/env bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
source config.env.sh

cd ..
VERSION=$(cat VERSION)
VERSION_TAG=${RELEASE_TAG_PREFIX}${VERSION}${RELEASE_TAG_POSTFIX}
TAG_OPTS="-t $IMAGE_URI:$VERSION_TAG -t $IMAGE_URI:latest"
docker build $TAG_OPTS -f Dockerfile .

